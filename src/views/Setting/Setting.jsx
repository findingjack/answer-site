import 'antd/dist/antd.min.css';
import './Setting.css';
import { React } from 'react';
import { Layout, PageHeader, Button, Form, InputNumber, Switch } from 'antd';
const { Content } = Layout;

export default function Setting({ defaultTime, setTime, isShowA, setShowA }) {
  // console.log(defaultTime, setTime,typeof defaultTime);

  function INChange(value) {
    if (!value) {
      console.log('设为0');
      setTime(0);
    } else {
      setTime(value)
    }
  }

  return (
    <Content
      className="set-content"
      style={{
        margin: '24px 100px',
        padding: 24,
        minHeight: document.documentElement.clientHeight - 64 - 48,
      }}
    >
      <div className='setting'>
        <PageHeader
          className="site-page-header"
          title="全局配置"
        />
        <Form
          labelCol={{
            span: 10,
          }}
          wrapperCol={{
            span: 4,
          }}
          layout="horizontal"
          // initialValues={{
          //   size: componentSize,
          // }}
          // onValuesChange={onFormLayoutChange}
          size='middle'
          style={{
            marginTop: '20px'
          }}
        >
          <Form.Item
            label="倒计时设置(秒)"
          >
            <InputNumber
              // defaultValue={defaultTime}
              value={defaultTime}
              onChange={INChange}
            />
          </Form.Item>
          <Form.Item
            label="倒计时自动显示答案"
            valuePropName="checked">
            <Switch checked={isShowA} checkedChildren='开' unCheckedChildren='关' onChange={() => { setShowA(!isShowA) }} />
          </Form.Item>
          <Form.Item
            label=" "
            colon={false}
            labelCol={{ span: 9 }}
          >
            <Button
              type="primary"
              onClick={() => { }}
            >保存</Button>
          </Form.Item>
        </Form>
      </div>
    </Content>

  )
}
