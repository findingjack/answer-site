import Home from './Home/Home';
import Answer from './Answer/Answer';
import Setting from './Setting/Setting';
export {
  Home,
  Answer,
  Setting
}