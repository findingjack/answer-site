import 'antd/dist/antd.min.css';
import './Home.css';
import React from 'react';
import { useNavigate } from 'react-router-dom'
import { Layout, Button } from 'antd';
const { Content } = Layout;

export default function Home() {
  const navigate = useNavigate() //利用重定向，跳到答题页面
  return (
    <Content
      className="site-layout-background home-content"
      style={{
        margin: '24px 16px',
        padding: 24,
        // 此处决定显示高度，计算下设置全屏最小高度，屏幕高度减去导航栏高度和margin
        minHeight: document.documentElement.clientHeight - 64 - 48,
        background: 'url(' + process.env.PUBLIC_URL + 'homeBG.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center 0',
        position: 'relative'
      }}
    >
      <div className='Home'>
        <Button
          type="primary"
          className='startBtn'
          onClick={()=>{
            navigate('/answer')
          }}
        >开始答题</Button>
      </div>
    </Content>

  )
}
