// 引入数据
// import tempData from '../../data/questionData';
import '../../mock/mock'
import axios from 'axios';
// 引入封装的progress
import Progress from '../../components/Progress/Progress';

import 'antd/dist/antd.min.css';
import './Answer.css';
import React, { useState, useEffect } from 'react';
import {
  Layout,
  Button,
  Radio,
  Checkbox,
  Input,
  Modal
} from 'antd';
const { Content } = Layout;


export default function Answer({ defaultTime, isShowA }) {
  let [nowIndex, setIndex] = useState(0) //定义状态，来控制切题
  let [questionData, setData] = useState({ data: [] })
  const [isModalVisible, setIsModalVisible] = useState(false); //控制对话框显隐
  // 在挂载阶段请求题目数据
  useEffect(() => {
    axios.get('/question').then(res => {
      // console.log(res.data);
      setData(res.data)
    })
  }, [])
  // console.log(tempData);

  // 渲染题目结构函数
  // 回头写一个状态，用来存储当前是第几道题
  // 该状态传到函数里面，渲染对应的题目
  function renderQuestion(questionData, index = 0) {
    // console.log(index,questionData.data);
    if (questionData.data.length > 0 && index <= questionData.data.length - 1) {
      // console.log(questionData);
      const data = questionData.data
      const currentQuestion = data[index]
      let questionType = null
      // console.log(currentQuestion);
      switch (currentQuestion.questionType) {
        case 1:
          questionType = '单选题'
          break;
        case 2:
          questionType = '多选题'
          break;
        case 3:
          questionType = '填空题'
          break;
        default:
          break;
      }
      // console.log(questionType,currentQuestion.options);
      return (
        <div key={currentQuestion.id}>
          <div className='Qu-title'>{index + 1}、{questionType}:{currentQuestion.title}</div>
          {renderOptions(currentQuestion.questionType, currentQuestion.options)}
          <div className='Qu-answer'>{currentQuestion.answer}</div>
          <div className='progress' key={currentQuestion.id}>
            <Progress seconds={defaultTime} isShowA={isShowA} setIsModalVisible={setIsModalVisible} />
          </div>
        </div>
      )
    } else if (questionData.data.length > 0) {
      return (
        <div className='no-data'>已做完全部题目</div>
      )
    } else {
      return (
        <div className='no-data'>暂无题目</div>
      )
    }

  }

  function renderOptions(type, content) {
    const option = ['A', 'B', 'C', 'D']
    // console.log(type, content);
    switch (type) {
      case 1:
        return <Radio.Group name='single-group' className='Qu-options'>{
          content.map((item, index) => <Radio name='choice' key={index} value={index}>{option[index]}:{item}</Radio>)
        }</Radio.Group>
      case 2:
        return <div className='Qu-options'>{
          content.map((item, index) => <Checkbox name='choice' key={index}>{option[index]}:{item}</Checkbox>)
        }</div>
      case 3: //case3 要修改成填空题形式
        return <div className='Qu-options'>{
          <Input style={{ marginTop: 10 }} placeholder='答案' size='large' />
        }</div>
      default:
        return <div>暂无选项</div>
    }
  }

  function backAnswer(data, index) {
    // console.log(data, index);
    if (data.length > 0 && index <= data.length - 1) {
      // console.log(data[index].answer);
      return data[index].answer
    } else {
      return '暂无答案'
    }
  }



  return (
    <Content
      className="site-layout-background answer-content"
      style={{
        margin: '24px 16px',
        padding: 24,
        // 此处决定显示高度，计算下设置全屏最小高度，屏幕高度减去导航栏高度和margin
        minHeight: document.documentElement.clientHeight - 64 - 48,
        background: 'url(' + process.env.PUBLIC_URL + 'answerBG.png)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center 0',
        position: 'relative'
      }}
    >
      <div className='problem-area'>
        {/* 题目区域,利用自定义函数完成对题目的渲染 */}
        {renderQuestion(questionData, nowIndex)}
        <Button
          type="primary"
          className='next-btn'
          onClick={() => { setIndex(nowIndex + 1) }}
        >下一题</Button>
        <Button
          type="primary"
          className='answer-btn'
          onClick={() => { setIsModalVisible(!isModalVisible) }}
        >查看答案</Button>
        <Modal
          title='答案'
          visible={isModalVisible}
          onCancel={() => { setIsModalVisible(!isModalVisible) }}
          onOk={() => { setIsModalVisible(!isModalVisible) }}
        >
          {backAnswer(questionData.data, nowIndex)}
        </Modal>
      </div>
    </Content>

  )
}