import 'antd/dist/antd.min.css';
import React, { useState, useEffect, useRef } from 'react';
import { Progress } from 'antd';

export default function MyProgress({ seconds = 15, isShowA, setIsModalVisible }) { //isShowA全局配置的是否显示，setIsModalVisible控制model弹出
  // 对象解构方式获取prop传递过来的倒计时起始值。
  const [isStart, setIsStart] = useState(false) //控制倒计时是否开始
  const [paused, setPaused] = useState(false) //控制倒计时是否暂停
  const [over, setOver] = useState(false) //控制倒计时是否结束
  // time 组件内自循环，控制倒计时
  const [time, setTime] = useState(parseInt(seconds));



  //定义时间滴答减少函数
  const tick = () => {
    // console.log('进入tick里面time的值', time, 'paused:', paused, 'over:', over, timeRef.current);
    // 暂停，或已结束
    if (!isStart || paused || over) return;
    if (time == 0) {
      // console.log('把over置零了');
      setOver(true);
    } else {
      // console.log('只是减一');
      setTime((time) => {  //useState 的更新为异步,旧值和新值渲染有时机问题,需要写成回调形式规避,最后发现是闭包陷阱的问题
        let newTime = time - 1;
        // console.log('Time状态里的newT和time', newTime, time);
      return newTime
      }); //为什么值不更新
    }
  }

  // 重置，后面要绑定下一题刷新上。
  // const reset = () => {
  //   setTime(parseInt(seconds));
  //   setPaused(false);
  //   setOver(false);
  // };


  const clickEve = () => {
    if (!isStart) {
      setIsStart(true)
    } else {
      setPaused(!paused)
    }
  }

  // 定时器必须要配合effect，要不然会有闭包问题
  useEffect(() => {
    // 执行定时
    let timerID = setInterval(() => tick(), 1000);
    // 状态更新后，如果设置显示且时间为零，就把答案展出
    if (isShowA && time===0) {
      setTime('0') //简化下只显示一次答案，将事件转为字符零，避免之后再进
      setIsModalVisible(true);
    }
    // 卸载组件时进行清理
    return () => clearInterval(timerID);
    // 触发时机不对，回调不是在组件销毁时触发，而是每次渲染完及销毁时，react就会把之前的effect销毁掉，执行return
  });

  return (
    <Progress
      type="circle"
      trailColor='red'
      status='normal'
      strokeLinecap="square"
      width={50}
      percent={time / seconds * 100}  //设计一个定时器，定时器时间除以设置答题时长，为此处的percent
      format={() => `${time.toString()}`} //这里要改成定时器秒数
      onClick={clickEve} //要把点击事件改为初次点击创建倒计时，后续点击暂停倒计时
    />
  )
}