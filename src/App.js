// 引入路由
import {
  Routes,
  Route,
  Navigate,
  NavLink as Link,
  Outlet,
  useLocation
} from 'react-router-dom';
// 引入子组件
import { Home, Answer, Setting } from "./views";
import './App.css'
// 引入antd
import 'antd/dist/antd.min.css'
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  SettingOutlined,
  UserOutlined,
  HighlightOutlined,
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { useState, useEffect } from 'react';
const { Header, Sider } = Layout;


const App = () => {
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation()
  const [selectedMenu, setMenu] = useState(['home'])
  // 要写个监听事件，让按键点击的路由跳转也能被监听到，然后改变菜单的选中状态
  useEffect(() => {
    // 设置菜单的selectedKeys永远和路由相一致
    setMenu(location.pathname)
  })
  // 倒计时的默认时间写在App里，通过受控组件的方式实现设置以及答题时间的对应修改。后面时间充裕升级为redux状态管理
  const [defaultTime, setTime] = useState(15) //默认倒计时15s
  const [isShowA, setShowA] = useState(true) //默认显示答案

  // setState没法直接传递给子组件，需要封装一下
  var childSetTime = (newTime) => {
    setTime(newTime)
  }

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed} collapsedWidth={0}>
        <div className="logo" >知识竞赛</div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['/home']}
          selectedKeys={selectedMenu}
          items={[
            {
              key: '/home',
              icon: <UserOutlined />,
              label: <Link to='/home'>首页</Link>,
            },
            {
              key: '/answer',
              icon: <HighlightOutlined />,
              label: <Link to='/answer'>答题</Link>,
            },
            {
              key: '/setting',
              icon: <SettingOutlined />,
              label: <Link to='/setting'>配置</Link>,
            },
          ]}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{
            padding: 0,
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}
        </Header>
        {/* 无匹配路由就跳转到首页 */}
        <Routes>
          <Route path='*' element={<Navigate to='/home' />}></Route>
          <Route exact path='/home' element={<Home />}></Route>
          <Route exact path='/answer' element={<Answer defaultTime={defaultTime} isShowA={isShowA} />}></Route>
          <Route exact path='/setting' element={<Setting defaultTime={defaultTime} setTime={setTime} isShowA={isShowA} setShowA={setShowA} />}></Route>
        </Routes>
        <Outlet />
        {/* <Content
          className="site-layout-background"
          style={{
            margin: '24px 16px',
            padding: 24,
            // 此处决定显示高度，计算下设置全屏最小高度，屏幕高度减去导航栏高度和margin
            minHeight: document.documentElement.clientHeight - 64 - 48,
            background: 'url(' + process.env.PUBLIC_URL + 'homeBG.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center 0',
            position: 'relative'
          }}
        >
        </Content> */}
      </Layout>
    </Layout >
  );


};

export default App;