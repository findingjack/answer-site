import Mock from 'mockjs'
var singleDate = Mock.mock({
  'data|5':[{
    'id|+1': 1, //设计一个自增数列，确定唯一id，进行答案比较
    'questionType':1, //1单选 2多选 3填空
    title:'@cparagraph(2, 4)', //题目标题
    options:[
      '@csentence(5, 10)',
      '@csentence(5, 10)',
      '@csentence(5, 10)',
      '@csentence(5, 10)'
    ],
    'answer|1':['A','B','C','D']
  }]
})
var multipleDate = Mock.mock({
  'data|5':[{
    'id|+1': 6, //设计一个自增数列，确定唯一id，进行答案比较
    'questionType':2, //1单选 2多选 3填空
    title:'@cparagraph(2, 4)', //题目标题
    options:[
      '@csentence(5, 10)',
      '@csentence(5, 10)',
      '@csentence(5, 10)',
      '@csentence(5, 10)'
    ],
    'answer|1':["AB", "AC", "AD", "BC","BD","CD","ABC","BCD","ABCD"]
  }]
})
var fillBlankDate = Mock.mock({
  'data|5':[{
    'id|+1': 11, //设计一个自增数列，确定唯一id，进行答案比较
    'questionType':3, //1单选 2多选 3填空
    title:'@cparagraph(2, 4)', //题目标题
    'answer':'@csentence(5, 10)'
  }]
})
Mock.mock('/question',{
  'data':[
    ...singleDate.data,
    ...multipleDate.data,
    ...fillBlankDate.data
  ]
})